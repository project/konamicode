/**
 * @file
 * JavaScript code for the Flip Text action.
 */

Drupal.konamicode_action_flip_text = function () {
  'use strict';
  jQuery('body').flipText();
};
