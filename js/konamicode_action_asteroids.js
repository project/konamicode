/**
 * @file
 * JavaScript code for the Asteroids action.
 */

Drupal.konamicode_action_asteroids = function () {
  'use strict';
  jQuery('body').startAsteroids();
};
