# Konami Code

The Konami Code is a cheat code that appeared in many Konami video games.
The Konami Code module makes it so that when users enter a given code on
your website, it invokes a certain action.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/konamicode).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/konamicode).


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

- drupal-konamicode/raptorize
- drupal-konamicode/flip_text
- drupal-konamicode/image_spawn
- drupal-konamicode/snowfall
- drupal-konamicode/asteroids


## Installation

It is recommended to install this module using composer, this will also download
all the dependencies on external libraries that this module has. To install the
module using composer execute: `composer require drupal/konamikode`

When developing on this module (meaning you did a git checkout) or when you
downloaded the module as a package from drupal.org, make sure to execute the
following composer commands:

- `composer require drupal-konamicode/raptorize:VERSION`
- `composer require drupal-konamicode/flip_text:VERSION`
- `composer require drupal-konamicode/image_spawn:VERSION`
- `composer require drupal-konamicode/snowfall:VERSION`
- `composer require drupal-konamicode/asteroids:VERSION`

Please check for the latest versions the composer.json file of this module.


## Configuration

You can manage all the actions and their specific configuration options from the
admin UI located at: `/admin/config/user-interface/konamicode`.


## Troubleshooting

For the complete documentation about the module please have a look at:
- [konami-code](https://www.drupal.org/docs/8/modules/konami-code)


## Maintainers

- Bram Driesen - [BramDriesen](https://www.drupal.org/u/BramDriesen)
- Dave Reid - [Dave Reid](https://www.drupal.org/u/dave-reid)
- Matt Farina - [mfer](https://www.drupal.org/u/mfer)
- Nick Veenhof - [Nick_vh](https://www.drupal.org/u/nick_vh)
- Rob Loach - [RobLoach](https://www.drupal.org/u/robloach)
- Wim Leers - [Wim Leers](https://www.drupal.org/u/wim-leers)