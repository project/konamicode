The Konami Code module is built and maintained by the Drupal project community.
Everyone is encouraged to submit issues and changes (patches) to improve it, and
to contribute in other ways -- see http://drupal.org/contribute to find out how.

Project maintainers
-------------------

The Konami Code module maintainers oversee the development of the project as a
whole. The project maintainers for Entity translation copy are:

- Bram Driesen 'BramDriesen' <https://www.drupal.org/u/BramDriesen>
